import unittest
from pyopenLooKeng import connections

_HOST = 'localhost'
_PORT = '8080'


class TestConnection(unittest.TestCase):
    __test__ = True
    def connect(self):
        return connections.connect(host=_HOST, port=_PORT, source=self.id())

    def test_bad_protocol(self):
        self.assertRaisesRegexp(ValueError, 'Protocol must be',
                                lambda: connections.connect('localhost', protocol='nonsense').cursor())

    def test_bad_https_verify(self):
        self.assertRaisesRegexp(
            ValueError, 'https_verify must be',
            lambda: connections.connect(host=_HOST, port=_PORT, protocol='https', https_verify='nonsense')
        )

    def test_cluster(self):
        conn = connections.Connection(host=_HOST, port=_PORT)
        self.assertIn('totalMemory', conn.cluster())

    def test_node(self):
        conn = connections.Connection(host=_HOST, port=_PORT)
        self.assertIn('uri', conn.node())

    def test_query(self):
        conn = connections.Connection(host=_HOST, port=_PORT)
        self.assertIn('clientTransactionSupport', str(conn.query()))
        # test queryID
        self.assertIn('queryId', str(conn.query('20210330_054757_00001_6hfui')))

    def test_stage(self):
        """
        There is a problem with the ``v1/stage`` interface
        """
        pass
