import time
from pyopenLooKeng import connections
import multiprocessing
import time
from numpy import *

_HOST = '82.156.184.5'
_PORT = '9090'


def func(data):
    start_time = time.time()
    conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                  username='test')
    cur = conn.cursor()
    cur.execute(
        "select * from mysql.test_olk.student"
    )
    res = cur.fetchall()
    end_time = time.time()
    data.append(end_time - start_time)


if __name__ == "__main__":
    for i in [ 4, 8, 16, 32, 64, 96 , 128]:
        pool = multiprocessing.Pool(i)
        data = multiprocessing.Manager().list()
        for _ in range(200):
            pool.apply_async(func, (data,))  # 维持执行的进程总数为processes，当一个进程执行完毕后会添加新的进程进去
        pool.close()
        pool.join()
        print("进程池大小：", i)
        print("最小用时", min(data))
        print("平均用时", mean(data))
        print("最长用时", max(data))
        print("总用时", sum(data))
        print("#############")
        time.sleep(30)
