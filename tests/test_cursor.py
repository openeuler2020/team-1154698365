import datetime
import unittest
from pyopenLooKeng import connections
from pyopenLooKeng.cursors import olkParamEscaper

_HOST = 'localhost'
_PORT = '8080'


class TestCursor(unittest.TestCase):
    __test__ = True

    def cursor(self):
        conn = connections.Connection(host=_HOST, port=_PORT)
        return conn.cursor()

    def test_fetchall(self):
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchall()
        self.assertEqual(
            '0831103', res[-1][0]
        )

    def test_fetchone(self):
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchone()
        self.assertEqual(
            '0811101', res[0]
        )

    def test_fetchmany(self):
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchmany(3)
        self.assertEqual(
            '0811103', res[2][0]
        )

    def test_description(self):
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchmany(3)
        des = cur.description
        self.assertEqual(
            'sname', des[1][0]
        )

    def test_cancel(self):
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        query_id = cur.last_query_id

        self.assertEqual('RUNNING', conn.query(query_id)['state'])
        cur.cancel()
        self.assertEqual('FAILED', conn.query(query_id)['state'])

    def test_escape_args(self):
        escaper = olkParamEscaper()

        self.assertEqual(escaper.escape_args((datetime.date(2021, 3, 17),)),
                         ("date '2021-03-17'",))
        self.assertEqual(escaper.escape_args((datetime.datetime(2021, 3, 17, 12, 0, 0, 123456),)),
                         ("timestamp '2021-03-17 12:00:00.123'",))

    def test_https(self):
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test', protocol='https', https_verify='trust')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchone()
        self.assertEqual(
            '0811101', res[0]
        )

        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test', protocol='https', https_verify='certificate')
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchone()
        self.assertEqual(
            '0811101', res[0]
        )
