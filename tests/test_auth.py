import unittest

from pyopenLooKeng import connections
from pyopenLooKeng.auth import *

_HOST = 'localhost'
_PORT = '8080'


class test_auth(unittest.TestCase):
    def test_invalid_protocol(self):
        auth = BasicAuthentication(username='user', password='password')
        self.assertRaisesRegexp(
            ValueError, 'https_verify must be',
            lambda: connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk',
                                           source='pythonClinet',
                                           username='test', protocol='http', auth=auth)
        )

    def test_BasicAuthentication(self):
        auth = BasicAuthentication(username='user', password='password')
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test', protocol='https', https_verify='trust', auth=auth)
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchone()
        self.assertEqual(
            '0811101', res[0]
        )

    def test_KerberosAuthentication(self):
        auth = KerberosAuthentication()
        conn = connections.Connection(host=_HOST, port=_PORT, catalog='mysql', schema='test_olk', source='pythonClinet',
                                      username='test', protocol='https', https_verify='trust', auth=auth)
        cur = conn.cursor()
        cur.execute(
            "select * from mysql.test_olk.student"
        )
        res = cur.fetchone()
        self.assertEqual(
            '0811101', res[0]
        )
