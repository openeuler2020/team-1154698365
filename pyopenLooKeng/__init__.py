"""
DB-API implementation backed by openLookeng
See http://www.python.org/dev/peps/pep-0249/
Many docstrings in this file are based on the PEP, which is in the public domain.
"""

# PEP 249 module globals
from pyopenLooKeng.connections import Connection

apilevel = '2.0'  # stating the supported DB API level.
threadsafety = 2  # Threads may share the module and connections.
paramstyle = 'pyformat'  # Python extended format codes, e.g. ...WHERE name=%(name)s


if __name__ == '__main__':
    conn = Connection(host="82.156.204.242", port=9090, catalog='mysql', schema='test_olk', source='pythonClinet',
                                  username='test')
    cur = conn.cursor()
    cur.execute(
        "select * from mysql.test_olk.student"
    )
    query_id = cur.last_query_id
    # print(conn.query(query_id))
    cur.cancel()
    print(conn.query(query_id))