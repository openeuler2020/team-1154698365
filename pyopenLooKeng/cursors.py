"""
DB-API implementation backed by openLooKeng
See http://www.python.org/dev/peps/pep-0249/
Many docstrings in this file are based on the PEP, which is in the public domain.
"""
import base64
import datetime
from . import err, common

try:  # Python 3
    import urllib.parse as urlparse
except ImportError:  # Python 2
    import urlparse


class olkParamEscaper(common.ParamEscaper):
    def escape_datetime(self, item, format):
        _type = "timestamp" if isinstance(item, datetime.datetime) else "date"
        formatted = super(olkParamEscaper, self).escape_datetime(item, format, 3)
        return "{} {}".format(_type, formatted)


_escaper = olkParamEscaper()


class Cursor(common.DBAPICursor):
    """
    This is the object used to interact with the database.
    Do not create an instance of a Cursor yourself. Call
    connections.Connection.cursor().
    See `Cursor <https://www.python.org/dev/peps/pep-0249/#cursor-objects>`_ in
    the specification.
    """

    def __init__(self, connection):
        self._connection = connection
        self.last_query_id = None
        self.poll_interval = self._connection.poll_interval
        self._reset_state()

    def mogrify(self, query, args=None):
        """
        Returns the exact string that would be sent to the database by calling the
        execute() method.
        :param query: Query to mogrify.
        :type query: str
        :param args: Parameters used with query. (optional)
        :type args: tuple, list or dict
        :return: The query with argument binding applied.
        :rtype: str
        This method follows the extension to the DB API 2.0 followed by Psycopg.
        """
        if args is not None:
            query = query % _escaper.escape_args(args)

        return query

    def _reset_state(self):
        """
        Reset state about the previous query in preparation for running another query
        """
        super(Cursor, self)._reset_state()
        self._nextUri = None

    def _fetch_more(self):
        """Fetch the next URI and update state"""
        self._process_response(self._connection.requests_session.get(self._nextUri, verify=self._connection.https_verify))

    @property
    def description(self):
        """
        This read-only attribute is a sequence of 7-item sequences.
        Each of these sequences contains information describing one result column:
        - name
        - type_code
        - display_size (None in current implementation)
        - internal_size (None in current implementation)
        - precision (None in current implementation)
        - scale (None in current implementation)
        - null_ok (always True in current implementation)
        The first two items (name and type_code) are mandatory,
        the other five are optional and are set to None if no meaningful values can be provided.
        """
        # Sleep until the query is done or we got the columns
        self._fetch_while(
            lambda: self._columns is None and
                    self._state not in (self._STATE_NONE, self._STATE_FINISHED)
        )
        if self._columns is None:
            return None
        return [
            # name, type_code, display_size, internal_size, precision, scale, null_ok
            (col['name'], col['type'], None, None, None, None, True)
            for col in self._columns
        ]

    def execute(self, operation, parameters=None):
        """
        Prepare and execute a database operation (query or command).
        Return values are not defined.
        """
        self._reset_state()
        self._state = self._STATE_RUNNING
        headers = {
            'X-Presto-Catalog': self._connection.catalog,
            'X-Presto-Schema': self._connection.schema,
            'X-Presto-Source': self._connection.source,
            'X-Presto-User': self._connection.username,
        }
        # Prepare statement
        operation = self.mogrify(operation, parameters)

        url = urlparse.urlunparse((
            self._connection.protocol,
            '{}:{}'.format(self._connection.host, self._connection.port), '/v1/statement', None, None, None))

        response = self._connection.requests_session.post(
            url=url,
            data=operation.encode('utf-8'),
            headers=headers,
            verify=self._connection.https_verify
        )
        self._process_response(response=response)
        self._fetch_more()  # Execute once, let the cluster start to run the query

    def _process_response(self, response):
        """
        Given the JSON response from openLooKeng's REST API, update the internal state with the next
        URI and any data from the response
        """
        if response.status_code != self._connection.requests_session.codes.ok:
            fmt = "Unexpected status code {}\n{}"
            raise err.OperationalError(fmt.format(response.status_code, response.content))

        response_json = response.json()
        assert self._state == self._STATE_RUNNING, "Should be running if processing response"
        self._nextUri = response_json.get('nextUri')
        self._columns = response_json.get('columns')
        if 'id' in response_json:
            self.last_query_id = response_json['id']
        if 'data' in response_json:
            assert self._columns
            new_data = response_json['data']
            self._decode_binary(new_data)
            self._data += map(tuple, new_data)
        if 'nextUri' not in response_json:
            self._state = self._STATE_FINISHED
        if 'error' in response_json:
            raise err.DatabaseError(response_json['error'])

    def _decode_binary(self, rows):
        """
        As of Presto 0.69, binary data is returned as the varbinary type in base64 format
        This function decodes base64 data in place
        """
        for i, col in enumerate(self.description):
            if col[1] == 'varbinary':
                for row in rows:
                    if row[i] is not None:
                        row[i] = base64.b64decode(row[i])

    def cancel(self):
        """
        Cancel a running query
        """
        if self._state == self._STATE_NONE:
            raise err.ProgrammingError("No query yet")

        if self._nextUri is None:
            assert self._state == self._STATE_FINISHED, "Should be finished if nextUri is None"
            return
        headers = {
            'X-Presto-Catalog': self._connection.catalog,
            'X-Presto-Schema': self._connection.schema,
            'X-Presto-Source': self._connection.source,
            'X-Presto-User': self._connection.username,
        }
        response = self._connection.requests_session.delete(self._nextUri, headers=headers, verify=self._connection.https_verify)
        if response.status_code != self._connection.requests_session.codes.no_content:
            fmt = "Unexpected status code after cancel {}\n{}"
            raise err.OperationalError(fmt.format(response.status_code, response.content))
        self._state = self._STATE_FINISHED
        self._nextUri = None
