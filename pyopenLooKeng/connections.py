"""
DB-API implementation backed by openLooKeng
See http://www.python.org/dev/peps/pep-0249/
Many docstrings in this file are based on the PEP, which is in the public domain.
"""

import getpass
import logging
import requests
from .cursors import Cursor
import certifi
from . import err

try:  # Python 3
    import urllib.parse as urlparse
except ImportError:  # Python 2
    import urlparse

logging.basicConfig(level=logging.WARNING,
                    format='[%(asctime)s] - [%(levelname)s]  - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S'
                    )


def connect(*args, **kwargs):
    """
    Constructor for creating a connection to the database. See class :py:class:`Connection` for
    arguments.
    :returns: a :py:class:`Connection` object.
    """
    return Connection(*args, **kwargs)


class Connection:
    """
    The proper way to get an instance of this class is to call connect().
    But openLooKeng does not have a notion of a persistent connection.
    Thus , set parameters related to RESTful requests here
    """

    def __init__(
            self,
            host,
            port=8080,
            username=None,
            catalog=None,
            schema=None,
            poll_interval=0.5,
            source=None,
            protocol='http',
            https_verify='trust',
            requests_session=None,
            auth=None,
    ):
        """
        :param host: string -- hostname to connect to, e.g. ``openLooKeng.example.com`` or ``127.0.0.9:8080``
        :param port: int -- port, defaults to 8080
        :param catalog: string -- Select the openLooKeng's connector to be connected,
            e.g. ``jmx`` or ``mysql ``
        :param schema: string -- Select the openLooKeng's schema to be connected,
            which is similar to the concept of mysql`s database
        :param poll_interval:float -- how often to ask the openLooKeng REST interface for a progress
            update, defaults to 0.5s
        :param source: string -- arbitrary identifier (shows up in the openLooKeng monitoring page)
        :param protocol: string -- network protocol, valid options are ``http`` and ``https``.
            defaults to ``http``
        :param https_verify:string -- valid options are ``trust`` and ``certificate``.
            ``trust`` means cancel SSL certification
            ``certificate`` means Use Mozilla's carefully planned root certificate collection provided by
            certifi.where().Please add the OpenLooKeng certificate to the .pem file before use
        :param requests_session: a ``requests.Session`` object for advanced usage. If absent, this
            class will use the default requests behavior of making a new session per HTTP request.
            Caller is responsible for closing session.
        :param auth : a ``Authentication`` object for managing user authentication.
            defalutts to ``None`` means no authentication.
        """
        self.host = host
        self.port = port

        self.username = username or getpass.getuser()
        self.catalog = catalog
        self.schema = schema

        self.poll_interval = poll_interval
        self.source = source

        # Determine whether the network protocol is http or https
        if protocol not in ('http', 'https'):
            raise ValueError("Protocol must be http/https, was {!r}".format(protocol))
        self.protocol = protocol

        # Handling HTTPS certificates
        if https_verify not in ("trust", "certificate"):
            raise ValueError("https_verify must be trust/certificate, was {!r}".format(protocol))
        if https_verify == "trust":
            self.https_verify = False
        if https_verify == "certificate":
            logging.warn("Before using the ``certificate`` mode, please make sure that the OpenLooKeng certificate "
                         "has been added to the ``certifi.where()`` file")
            self.https_verify = certifi.where()

        self.requests_session = requests_session or requests
        self.auth = auth
        if self.auth:
            if protocol == 'http':
                raise ValueError("cannot use authentication with HTTP protocol")
            self.auth.set_http_session(self.requests_session)

    def close(self):
        """
        openLooKeng does not have anything to close
        """
        pass

    def commit(self):
        """
        openLooKeng does not support transactions
        """
        pass

    def cursor(self):
        """
        Return a new :py:class:`Cursor` object using the connection.
        """
        return Cursor(self)

    def rollback(self):
        raise err.NotSupportedError("openLooKeng does not have transactions")

    def cluster(self):
        """
        Get information about the cluster
        :return: json str
        """
        url = urlparse.urlunparse((
            self.protocol,
            '{}:{}'.format(self.host, self.port), '/v1/cluster', None, None, None))
        response = self.requests_session.get(url=url, verify=self.https_verify)
        return response.json()

    def node(self):
        """
        Get information about working nodes
        :return: json str
        """
        url = urlparse.urlunparse((
            self.protocol,
            '{}:{}'.format(self.host, self.port), '/v1/node', None, None, None))
        response = self.requests_session.get(url=url, verify=self.https_verify)
        return response.json()

    def query(self, queryId=None):
        if queryId == None:
            """
            This service returns information and statistics about the queries currently executed on the openLooKeng 
            coordination node.
            """
            url = urlparse.urlunparse((
                self.protocol,
                '{}:{}'.format(self.host, self.port), '/v1/query', None, None, None))
            response = self.requests_session.get(url=url, verify=self.https_verify)
            return response.json()
        else:
            """
            If you are looking to gather very detailed statistics about a query, this is the service you would call. 
            """
            url = urlparse.urlunparse((
                self.protocol,
                '{}:{}'.format(self.host, self.port), '/v1/query/{}'.format(queryId), None, None, None))
            response = self.requests_session.get(url=url, verify=self.https_verify)
            return response.json()

    def stage(self, stageId=None):
        """
        The stage interface is currently unavailable, please see https://gitee.com/openlookeng/hetu-core/issues/I3EGT5?from=project-issue for details
        """

        if stageId == None:
            """
            This service returns information and statistics about the stages currently executed on the openLooKeng 
            coordination node.
            """
            url = urlparse.urlunparse((
                self.protocol,
                '{}:{}'.format(self.host, self.port), '/v1/stage', None, None, None))
            response = self.requests_session.get(url=url, verify=self.https_verify)
            return response.json()
        else:
            """
            If you are looking to gather very detailed statistics about a stage, this is the service you would call. 
            """
            url = urlparse.urlunparse((
                self.protocol,
                '{}:{}'.format(self.host, self.port), '/v1/stage/{}'.format(stageId), None, None, None))
            response = self.requests_session.get(url=url, verify=self.https_verify)
            return response.json()
