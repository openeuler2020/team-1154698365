# 62-穿梭在银河的火箭队

#### Description

Pyopenlookeng is the python db-api client of openlookeng.

TOPIC_ID:62, TEAM_ID:1154698365, TEAM_NAME:穿梭在银河的火箭队.

#### Software Architecture

##### Background
The Python client can also use RESTful communication, encapsulate the declaration of OpenLooKeng application into a RESTful request, and send it to the coordinator for execution, including the following request:
- Submit query request POST /v1/statement
- Query until the query is completed: GET /v1/statement/{ID}
- Delete someone’s query: DELETE /v1/statement/{queryId}

OpenLooKeng needs to provide encryption and authentication functions, and needs to support HTTPS encryption and basic and Kerberos authentication. As a module of OpenLooKeng, the Python client needs to comply with OpenLooKeng's development specifications. At the same time, the client needs to pass full tests, and the unit test coverage needs to meet the requirements of OpenLooKeng, and the function is completed, and it can access OpenLooKeng's business normally.

##### Pyopenlookeng adopts the pep-249v2.0 database API specification

PEP-249 DB-API provides a consistent access interface for different databases. For example, we can easily migrate the accessed database from Mysql to openlookeng with only a few code changes.

![flow](imgs/flow.png)


##### Architecture

![arch](imgs/architecture.png)

- auth: The architecture is used to obtain the request session, which is verified by HTTPBasic or Kerberos;
- err: Contains errors and warnings that may be encountered during the operation;
- common: Some common DB-API logic base classes;
- connections: Manage openLookeng connection to get cursor, set encrypted authentication information, etc;
- cursor: cursor represents a database cursor that manages the context of the extraction operation.

##### Function Description

**Connnection**

1. .cursor():Create openlookeng cursor instance
2. .cluster():Get json str of cluster information
3. .node(): Get work node information
4. .query(queryId=None): Get data information for all queries or specified queryId queries
5. .stage(stageId=None): Gets the data information of all stages or the specified stageId stage,The stage interface is currently unavailable, please see https://gitee.com/openlookeng/hetu-core/issues/I3EGT5?from=project-issue for details

**Cursor**

1. .description: Description information of database column type and value
2. .execute(operation[, parameters])：Execute database operations, SQL statements or database commands
3. .fetchone()：Gets the next record in the query result set
4. .fetchmany(size)：Gets the specified number of records
5. .fetchall()：Gets all records of the result set
6. .rowcount：Return the row count statistics of the result
7. .cancel(): Cancels a running query

#### Installation

1.  clone this repo and enter the catalog
```
git clone https://gitee.com/openeuler2020/team-1154698365.git && cd team-1154698365
```
2.  Install dependent libraries (please make sure it is python3 environment)
```
pip install -r requirements.txt
```
3.  Install pyopenLooKeng
```
python setup.py install
```
#### Instructions

1. Http connections without authentication
```python
from pyopenLooKeng import connections

conn = connections.connect(host='host', port=8080, catalog='system', schema='runtime')
print(conn.cluster())
print(conn.query())
# ...
cur = conn.cursor()
cur.execute("SHOW TABLES")
res = cur.fetchall()
print(res)
# (('nodes',), ('queries',), ('tasks',), ('transactions',))
```

2. Https connections
```python
from pyopenLooKeng import connections

conn = connections.connect(host='host', port=8080, catalog='system', schema='runtime',
    protocol="https",https_verify="trust")
print(conn.cluster())
print(conn.query())
# ...
cur = conn.cursor()
cur.execute("SHOW TABLES")
res = cur.fetchall()
print(res)
# (('nodes',), ('queries',), ('tasks',), ('transactions',))
```

3. HTTPS connections with authentication
```python
from pyopenLooKeng import connections, auth

myAuth = auth.BasicAuthentication(username='user', password='password')
# Kerberos authentication can also be used
conn = connections.connect(host='host', port=8080, catalog='system', schema='runtime',
                           protocol="https", https_verify="trust", auth=myAuth)
print(conn.cluster())
print(conn.query())
# ...
cur = conn.cursor()
cur.execute("SHOW TABLES")
res = cur.fetchall()
print(res)
# (('nodes',), ('queries',), ('tasks',), ('transactions',))

```
##### How to test pyopenLooKeng
1. Configure mysql Catalog in openLooKeng, we have prepared a sql file as test data, located in the ``tests/``directory
2. The test code files are all in the ``tests/`` directory, select the part you want to test, and configure the host and port of the code
3. Use the `py.test test_xxx.py` command to test

We added a new stress test code, and tested 200 mysql queries with different process pool sizes on the stand-alone OepnLooKeng.
Machine performance: 2 cores 4GB 5Mbps
Operating system: Ubuntu 18.04 LTS
The number of processes increases from small to large

Results:

| Process pool size | Minimum query time (s) | Average time       | Longest time       | Total Time         |
| ----------------- | ---------------------- | ------------------ | ------------------ | ------------------ |
| 4                 | 0.5312347412109375     | 0.7557646048069    | 1.1501729488372803 | 151.15292096138    |
| 8                 | 0.5318071842193604     | 0.8178671360015869 | 1.2911303043365479 | 163.57342720031738 |
| 16                | 0.5414032936096191     | 0.962301059961319  | 1.4733026027679443 | 192.4602119922638  |
| 32                | 0.5549044609069824     | 1.7005435049533844 | 3.0190680027008057 | 340.1087009906769  |
| 64                | 0.8101632595062256     | 3.232089376449585  | 10.673005104064941 | 646.417875289917   |
| 96                | 1.0760657787322998     | 5.666961643695831  | 12.587688684463501 | 1133.3923287391663 |
| 128               | 1.1646084785461426     | 7.965008200407028  | 13.491569519042969 | 1593.0016400814056 |

![image-20210420172716942](https://image-bed113224.oss-cn-beijing.aliyuncs.com/img/image-20210420172716942.png)

#### TODO List

- [x] Pressure test
- [ ] Upload this project to PyPI

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)