#!/usr/bin/env python
from setuptools import setup
setup(
    name='pyopenLooKeng',
    version='0.0.1',
    description='pyopenLooKeng 是 openLooKeng 的python DB-API客户端',
    url='https://gitee.com/openeuler2020/team-1154698365',
    author='62-穿梭在银河的火箭队',
    author_email='jiajingzhe1350@qq.com',
    license='MIT',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Natural Language :: Chinese (Simplified)',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Utilities'
    ],
    keywords='openLooKeng client python',
    packages=['pyopenLooKeng'],
)
