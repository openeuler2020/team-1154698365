# 62-穿梭在银河的火箭队

#### 介绍
pyopenLooKeng 是 openLooKeng 的python DB-API客户端

TOPIC_ID:62, TEAM_ID:1154698365, TEAM_NAME:穿梭在银河的火箭队.

#### 软件架构
##### 背景分析
OpenLooKeng采用REST通信,包括CLI、JDBC与 Coordinator, Coordinator 与 Worker。Python client也可以使用REST通信，将Statement 按照 OpenLooKeng求封装成REST请求,发送恰 Coordinator执行,包含以下请求：
- 提交查询请求 POST /v1/statement
- 查询直至查询完成： GET /v1/statement/{ID}
- 删除某个查询：DELETE /v1/statement/{queryId}

同时，OpenLooKeng需要提供加密和认证功能，需要支持HTTPS加密以及Basic与Kerberos认证。
Python客户端作为OpenLooKeng的模块，需要符合OpenLooKeng的开发规范，同时客户端需要经过充分测试，单元测试覆盖率需要符合OpenLooKeng的要求，并且功能完成，能够正常访问OpenLooKeng的业务。

##### pyopenLooKeng采用了PEP-249v2.0 数据库API规范
PEP-249 DB-API为不同的数据库提供了一致的访问接口，比如我们可以很轻松地将访问的数据库从Mysql等移植到OpenLooKeng上，只需要进行少量的代码更改。

![flow](imgs/flow.png)

##### 整体架构

![arch](imgs/architecture.png)

- auth: 架构用于获取请求会话，该会话由HTTPBasic或Kerberos验证;
- err: 包含操作期间可能遇到的错误和警告;
- common: 一些常见的DB-API逻辑的基类;
- connections: 管理openLooKeng连接，用于获取游标，设置加密的身份验证信息等;
- cursor: cursor表示数据库游标，用于管理提取操作的上下文。

##### 功能说明
**Connnection**
1. .cursor():创建OpenLooKeng游标实例
2. .cluster():获取集群信息的json str
3. .node(): 获取工作节点信息
4. .query(queryId=None): 获取所有查询或指定queryId查询的数据信息
5. .stage(stageId=None): 获取所有阶段或指定stageId阶段的数据信息,目前Stage接口无法使用,详情请见：https://gitee.com/openlookeng/hetu-core/issues/I3EGT5?from=project-issue

**Cursor**
1. .description: 数据库列类型和值的描述信息
2. .execute(operation[, parameters])：执行数据库操作，SQL语句或者数据库命令
3. .fetchone()：获取查询结果集中的下一条记录
4. .fetchmany(size)：获取指定数量的记录
5. .fetchall()：获取结果集的所有记录
6. .rowcount：回返结果的行数统计信息
7. .cancel(): 取消一个正在运行的查询

#### 安装教程

1.  下载本仓库并且进入到目录
```
git clone https://gitee.com/openeuler2020/team-1154698365.git && cd team-1154698365
```
2.  安装依赖库（请确保是python3环境）
```
pip install -r requirements.txt
```
3.  安装pyopenLooKeng
```
python setup.py install
```

#### 使用说明
1. 无认证的HTTP链接
```python
from pyopenLooKeng import connections

conn = connections.connect(host='host', port=8080, catalog='system', schema='runtime')
print(conn.cluster())
print(conn.query())
# ...
cur = conn.cursor()
cur.execute("SHOW TABLES")
res = cur.fetchall()
print(res)
# (('nodes',), ('queries',), ('tasks',), ('transactions',))
```

2. Https 连接
```python
from pyopenLooKeng import connections

conn = connections.connect(host='host', port=8080, catalog='system', schema='runtime',
    protocol="https",https_verify="trust")
print(conn.cluster())
print(conn.query())
# ...
cur = conn.cursor()
cur.execute("SHOW TABLES")
res = cur.fetchall()
print(res)
# (('nodes',), ('queries',), ('tasks',), ('transactions',))
```

3. 带有认证的Https 连接
```python
from pyopenLooKeng import connections, auth

myAuth = auth.BasicAuthentication(username='user', password='password')
# Kerberos authentication can also be used
conn = connections.connect(host='host', port=8080, catalog='system', schema='runtime',
                           protocol="https", https_verify="trust", auth=myAuth)
print(conn.cluster())
print(conn.query())
# ...
cur = conn.cursor()
cur.execute("SHOW TABLES")
res = cur.fetchall()
print(res)
# (('nodes',), ('queries',), ('tasks',), ('transactions',))

```
##### 如何对pyopenLooKeng进行测试
1. 在openLooKeng中配置mysql Catalog,我们准备了一份sql文件，作为测试数据，位于tests/目录下
2. 测试代码文件都在tests/目录下，选择想要测试的部分，并且对代码的host和port进行配置
3. 使用`py.test test_xxx.py`命令进行测试

我们增加了新的压力测试的代码，并且对单机的OepnLooKeng进行了不同进程池大小进行200次mysql查询的测试。
机器性能：2核 4GB 5Mbps
操作系统：Ubuntu 18.04 LTS
进程数由小到大递增

结果：

| 进程池大小 | 查询最小用时(s)    | 平均用时           | 最长用时           | 总用时             |
| ---------- | ------------------ | ------------------ | ------------------ | ------------------ |
| 4          | 0.5312347412109375 | 0.7557646048069    | 1.1501729488372803 | 151.15292096138    |
| 8          | 0.5318071842193604 | 0.8178671360015869 | 1.2911303043365479 | 163.57342720031738 |
| 16         | 0.5414032936096191 | 0.962301059961319  | 1.4733026027679443 | 192.4602119922638  |
| 32         | 0.5549044609069824 | 1.7005435049533844 | 3.0190680027008057 | 340.1087009906769  |
| 64         | 0.8101632595062256 | 3.232089376449585  | 10.673005104064941 | 646.417875289917   |
| 96         | 1.0760657787322998 | 5.666961643695831  | 12.587688684463501 | 1133.3923287391663 |
| 128        | 1.1646084785461426 | 7.965008200407028  | 13.491569519042969 | 1593.0016400814056 |

![image-20210420172716942](https://image-bed113224.oss-cn-beijing.aliyuncs.com/img/image-20210420172716942.png)

#### TODO-List
- [x] 进行压力测试
- [ ] 将本项目发布到PyPI

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
